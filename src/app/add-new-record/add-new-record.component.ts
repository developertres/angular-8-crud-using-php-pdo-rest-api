import {Component, Inject, OnInit} from '@angular/core';
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE, MAT_DIALOG_DATA, MatDialogRef, MatSnackBar} from '@angular/material';
import {MAT_MOMENT_DATE_FORMATS, MomentDateAdapter} from '@angular/material-moment-adapter';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import * as moment from 'moment';
export const MY_FORMATS = {
  parse: {
    dateInput: 'LL',
  },
  display: {
    dateInput: 'DD-MMM-YYYY',
    monthYearLabel: 'YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MM YYYY',
  },
};
export interface DialogData {
  data: any;
}

@Component({
  selector: 'app-add-new-record',
  templateUrl: './add-new-record.component.html',
  styleUrls: ['./add-new-record.component.scss'],
  providers: [
    {provide: MAT_DATE_LOCALE, useValue: 'en-ES'},
    {provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE]},
    {provide: MAT_DATE_FORMATS, useValue: MY_FORMATS},
  ]
})
export class AddNewRecordComponent implements OnInit {
  registerForm: FormGroup;
  maxiDate = moment().subtract(0, 'days').format('YYYY-MM-DD'); // Disable Future dates form Date picker


  constructor(public dialogRef: MatDialogRef<AddNewRecordComponent>,
              @Inject(MAT_DIALOG_DATA) public data: DialogData,
              private fb: FormBuilder) { }

  ngOnInit() {
    this.createRegisterForm();
  }

  dialogClose() {
    this.dialogRef.close('close');
  }
  createRegisterForm() {
    this.registerForm = this.fb.group({
      emp_name: ['', Validators.compose([Validators.required])],
      emp_mobile: ['', Validators.compose([Validators.required, Validators.pattern('[0-9]+$')])],
      emp_email: ['', Validators.compose([Validators.required, Validators.email])],
      emp_role: ['', Validators.compose([Validators.required])],
      emp_dob: ['', Validators.compose([Validators.required])]
    });
  }

  registerFormSubmit(value: any) {
    // value.emp_dob = moment(value.emp_dob).format('DD/MM/YYYY');
    value.emp_status = 1;
    value.emp_id = 'EMP_0000';
    this.dialogRef.close(value);
  }

}
