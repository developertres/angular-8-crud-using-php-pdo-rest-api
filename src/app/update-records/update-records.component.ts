import {Component, Inject, OnInit} from '@angular/core';
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE, MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MAT_MOMENT_DATE_FORMATS, MomentDateAdapter} from '@angular/material-moment-adapter';
import * as moment from 'moment';
export const MY_FORMATS = {
  parse: {
    dateInput: 'LL',
  },
  display: {
    dateInput: 'DD-MMM-YYYY',
    monthYearLabel: 'YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MM YYYY',
  },
};
@Component({
  selector: 'app-update-records',
  templateUrl: './update-records.component.html',
  styleUrls: ['./update-records.component.scss'],
  providers: [
    {provide: MAT_DATE_LOCALE, useValue: 'en-ES'},
    {provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE]},
    {provide: MAT_DATE_FORMATS, useValue: MY_FORMATS},
  ]
})
export class UpdateRecordsComponent implements OnInit {
  registerForm: FormGroup;
  userId: any;
  maxiDate = moment().subtract(0, 'days').format('YYYY-MM-DD'); // Disable Future dates form Date picker
  constructor(@Inject(MAT_DIALOG_DATA) public data: UpdateRecordsComponent,
              public dialogRef: MatDialogRef<UpdateRecordsComponent>,
              private fb: FormBuilder) {
    this.createRegisterForm();
  }

  ngOnInit() {
    this.updateFormValues(this.data);
  }
  updateFormValues(value) {
    this.userId = value.id;
    this.registerForm.controls.emp_id.setValue(value.emp_id);
    this.registerForm.controls.emp_name.setValue(value.emp_name);
    this.registerForm.controls.emp_role.setValue(value.emp_role);
    this.registerForm.controls.emp_mobile.setValue(value.emp_mobile);
    this.registerForm.controls.emp_email.setValue(value.emp_email);
    this.registerForm.controls.emp_dob.setValue(value.emp_dob);
  }
  createRegisterForm() {
    this.registerForm = this.fb.group({
      emp_id: ['', {disabled: true}],
      emp_name: ['', Validators.compose([Validators.required])],
      emp_mobile: ['', Validators.compose([Validators.required, Validators.pattern('[0-9]+$')])],
      emp_email: ['', Validators.compose([Validators.required, Validators.email])],
      emp_role: ['', Validators.compose([Validators.required])],
      emp_dob: ['', Validators.compose([Validators.required])]
    });
  }

  dialogClose() {
    this.dialogRef.close('close');
  }

  updateFormSubmit(value: any) {
    value.id = this.userId;
    this.dialogRef.close(value);
  }
}
