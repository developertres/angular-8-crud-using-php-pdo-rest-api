import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {ReadService} from '../_services/read.service';
import {NgxSpinnerService} from 'ngx-spinner';

@Component({
  selector: 'app-view-records',
  templateUrl: './view-records.component.html',
  styleUrls: ['./view-records.component.scss']
})
export class ViewRecordsComponent implements OnInit {
  userDetails: any;

  constructor(@Inject(MAT_DIALOG_DATA) public data: ViewRecordsComponent,
              private readService: ReadService,
              private spinner: NgxSpinnerService) { }

  ngOnInit() {
    this.spinner.show('viewSpinner',  {
      type: 'line-scale-party',
      size: 'large',
      bdColor: 'rgba(100,149,237, .8)',
      color: 'white'
    });
    this.readService.getSingleUserDetails(this.data).subscribe(res => {
      this.spinner.hide('viewSpinner');
      this.userDetails = res[0];
    });
  }
}
