import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {
  MatButtonModule, MatDatepickerModule,
  MatDialogModule,
  MatIconModule,
  MatInputModule, MatNativeDateModule,
  MatPaginatorModule, MatSelectModule, MatSnackBarModule,
  MatSortModule,
  MatTableModule
} from '@angular/material';
import { AddNewRecordComponent } from './add-new-record/add-new-record.component';
import { UpdateRecordsComponent } from './update-records/update-records.component';
import {MAT_MOMENT_DATE_ADAPTER_OPTIONS, MatMomentDateModule} from '@angular/material-moment-adapter';
import { ViewRecordsComponent } from './view-records/view-records.component';
import {NgxSpinnerModule} from "ngx-spinner";

@NgModule({
  declarations: [
    AppComponent,
    AddNewRecordComponent,
    UpdateRecordsComponent,
    ViewRecordsComponent
  ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        BrowserAnimationsModule,
        FormsModule,
        ReactiveFormsModule,
        HttpClientModule,
        MatTableModule,
        MatSortModule,
        MatButtonModule,
        MatPaginatorModule,
        MatInputModule,
        MatDialogModule,
        MatIconModule,
        MatSelectModule,
        MatNativeDateModule,
        MatDatepickerModule,
        MatMomentDateModule,
        MatSnackBarModule,
        NgxSpinnerModule
    ],
  entryComponents: [AddNewRecordComponent, UpdateRecordsComponent, ViewRecordsComponent],
  providers: [MatDatepickerModule],
  bootstrap: [AppComponent]
})
export class AppModule { }
