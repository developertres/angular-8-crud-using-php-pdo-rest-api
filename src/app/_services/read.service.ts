import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {from, Observable} from 'rxjs';
import {User} from '../_models/read.model';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ReadService {

  constructor(private http: HttpClient) { }
  getUserDetails() {
    return this.http.get<any>('https://www.thaaimozhikalvi.com/Youtube/Projects/angular_8_CRUD_operations/api_services/read.php').pipe(
      map(data => {
        return data;
      })
    );
  }
  getSingleUserDetails(id) {
    return this.http.get<any>('https://www.thaaimozhikalvi.com/Youtube/Projects/angular_8_CRUD_operations/api_services/read.php?id=' + id)
      .pipe(
      map(data => {
        return data;
      })
    );
  }
  insertRecord(req): Observable<any> {
    return this.http.post<any>('https://www.thaaimozhikalvi.com/Youtube/Projects/angular_8_CRUD_operations/api_services/insert.php', req).
    pipe(
      map(data => {
        return data;
      })
    );
  }
  deleteRecord(id) {
    return this.http.delete('https://www.thaaimozhikalvi.com/Youtube/Projects/angular_8_CRUD_operations/api_services/delete.php', id)
      .pipe(
      map(data => {
        return data;
      })
    );
  }
  disableUser(id): Observable<any> {
    return this.http.put('https://www.thaaimozhikalvi.com/Youtube/Projects/angular_8_CRUD_operations/api_services/disabled.php', id).pipe(
      map(data => {
        return data;
      })
    );
  }
  updateRecords(values): Observable<any> {
    return this.http.put('https://www.thaaimozhikalvi.com/Youtube/Projects/angular_8_CRUD_operations/api_services/update.php', values).pipe(
      map(data => {
        return data;
      })
    );
  }
}
